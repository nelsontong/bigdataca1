//-----------------------------------------------------------------------

// 1. INSERTING DOCUMENTS

db.movies.insert({
	title: "Fight Club",
	writer: "Chuck Palahniuk",
	year: 1999,
	actors: ["Brad Pitt", "Edward Norton"]
})


db.movies.insert({
	title: "Pulp Fiction",
	writer: "Quentin Tarantino",
	year: 1994,
	actors: ["John Travolta", "Uma Thurman"]
})


db.movies.insert({
	title: "Inglorious Basters",
	writer: "Quentin Tarantino",
	year: 2009,
	actors: ["Brad Pitt", "Diane Kruger", "Eli Roth"]
})


db.movies.insert({
	title: "The Hobbit: An Unexpected Journey",
	writer: "J.R.R. Tolkien",
	year: 2012,
	franchise: "The Hobbit"
})


db.movies.insert({
	title: "The Hobbit: The Desolation of Smaug",
	writer: "J.R.R. Tolkien",
	year: 2013,
	franchise: "The Hobbit"
})


db.movies.insert({
	title: "The Hobbit: The Battle of the Five Armies",
	writer: "J.R.R. Tolkien",
	year: 2012,
	franchise: "The Hobbit",
	synopsis: "Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness"
})


db.movies.insert({
	title: "Pee Wee Herman's Big Adventure"
})


db.movies.insert({
	title: "Avatar"
})

//-----------------------------------------------------------------------

// 2. FINDING DOCUMENTS

//Get all documents
db.movies.find()

//Get all documents with 'writer' set to "Quentin Tarantino"
db.movies.find({
	writer: "Quentin Tarantino"
})

//Get all documents where 'actors' include "Brad Pitt"
db.movies.find({
	actors: "Brad Pitt"
})

//Get all documents with 'franchise' set to "The Hobbit"
db.movies.find({
	franchise: "The Hobbit"
})

//Get all movies release in the 90s
db.movies.find({
	year: {$gt: 1990, $lt: 2000}
})

//Get all movies released before the year 2000 or after 2010
db.movies.find({ 
	$or: [
		{year: {$lt: 2000}},
		{year: {$gt: 2010}}
	]
})

// ----------------------------------------------------------------------

// 3. UPDATE DOCUMENTS

//Add synopsis to "The Hobbit: An Unexpected Journey"
db.movies.update(
	{ _id : ObjectId("581d15bdad9fe38f7ce00d2f") },
	{$set: {synopsis: "A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a sprited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug"}}
)

//Add a synopsis to "The Hobbit: The Desolation of Smaug"
db.movies.update(
	{ _id : ObjectId("581d15c1ad9fe38f7ce00d30") },
	{$set: {synopsis: "The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring"}}
)

//Add an actor named "Samuel L. Jackson" to the movie "Pulp Fiction"
db.movies.update(
	{ _id: ObjectId("581d15b5ad9fe38f7ce00d2d") },
	{ $push : { actors : "Samuel L. Jackson"}}
)

// ----------------------------------------------------------------------

// 4. TEXT SEARCH

//Find all movies that have a synopsis that contains the word "Bilbo"
db.movies.find({
	synopsis: {
		$regex: /Bilbo/
	}
})

//Find all movies that have a synopsis that contains the word "Gandalf"
db.movies.find({
	synopsis: {
		$regex: /Gandalf/
	}
})

//Find all movies that have a synopsis that contains the word "Bilbo" and not the word Gandalf
db.movies.find({
	synopsis: {
		$regex: /Bilbo/,
		$not: /Gandalf/
	}
})

//Find all movies that have a synopsis that contains the word "dwarves" or the word "hobbit"
db.movies.find({
	$or : [
		{ synopsis : { $regex : /dwarves/}},
		{ synopsis : { $regex : /hobbit/}}
	]
})

//Find all movies that have a synopsis that contains the word "gold" or the word "dragon"
db.movies.find({
	$or : [
		{ synopsis : { $regex : /gold/}},
		{ synopsis : { $regex : /dragon/}}
	]
})

// ----------------------------------------------------------------------

// 5. DELETE DOCUMENTS

//Delete the movie "Pee Wee Herman's Big Adventure"
db.movies.remove({
	_id : ObjectId("581d15cbad9fe38f7ce00d32") 
})

//Delete the movie "Avatar"
db.movies.remove({
	_id : ObjectId("581d15d0ad9fe38f7ce00d33") 
})

// ----------------------------------------------------------------------

// 6. RELATIONSHIPS

//Insert into 'users' collection
db.users.insert({
	username: "GoodGuyGreg",
	first_name: "Good Guy",
	last_name: "Greg"
})

db.users.insert({
	username: "ScumbagSteve",
	first_name: "Scumbag",
	last_name: "Steve"
})

//Insert into 'posts' collection
db.posts.insert({
	username: "GoodGuyGreg",
	title: "Passes out at party",
	body: "Wakes up early and cleans house"
})

db.posts.insert({
	username: "GoodGuyGreg",
	title: "Steals your identity",
	body: "Raises your credit score"
})

db.posts.insert({
	username: "GoodGuyGreg",
	title: "Reports a bug in your code",
	body: "Sends you a Pull Request"
})

db.posts.insert({
	username: "ScumbagSteve",
	title: "Borrows something",
	body: "Sells it"
})

db.posts.insert({
	username: "ScumbagSteve",
	title: "Borrows everything",
	body: "The end"
})

db.posts.insert({
	username: "ScumbagSteve",
	title: "Forks your repo on github",
	body: "Sets to private"
})

//Insert into 'comments' collection
db.comments.insert({
	username: "GoodGuyGreg",
	comment: "Hope you got a good deal!",
	post: "581e50227f324f608375807d"
})

db.comments.insert({
	username: "GoodGuyGreg",
	comment: "What's mine is yours!",
	post: "581e50367f324f608375807e"
})

db.comments.insert({
	username: "GoodGuyGreg",
	comment: "Don't violate the licensing agreement!",
	post: "581e52c37f324f6083758080"
})

db.comments.insert({
	username: "ScumbagSteve",
	comment: "It still isn't clean",
	post: "581e4f797f324f608375807a"
})

db.comments.insert({
	username: "ScumbagSteve",
	comment: "Denied you PR cause I found a hack",
	post: "581e4ff67f324f608375807c"
})

// ----------------------------------------------------------------------

// 7. QUERYING RELATED COLLECTIONS

//Find all users
db.users.find()

//Find all posts
db.posts.find()

//Find all posts that was authored by "GoodGuyGreg"
db.posts.find({
	username: "GoodGuyGreg"
})

//Find all posts that was authored by "ScumbagSteve"
db.posts.find({
	username: "ScumbagSteve"
})

//Find all comments
db.comments.find()

//Find all comments that was authored by "GoodGuyGreg"
db.comments.find({
	username: "GoodGuyGreg"
})

//Find all comments that was authored by "ScumbagSteve"
db.comments.find({
	username: "ScumbagSteve"
})

//Find all comments belonging to the post "Reports a bug in your code"
db.comments.find({
	post: "581e4ff67f324f608375807c"
})